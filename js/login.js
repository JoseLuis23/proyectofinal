
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
  import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js'
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyC63_-sO6wOagfb8UkyPvsOJgoPmDAp_HQ",
    authDomain: "proyectofinaljoseluis.firebaseapp.com",
    projectId: "proyectofinaljoseluis",
    storageBucket: "proyectofinaljoseluis.appspot.com",
    messagingSenderId: "217855224157",
    appId: "1:217855224157:web:e2ef12a6b055df273da0b4"
  };

  // Initialize Firebase
 // Inicializa Firebase
const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth(firebaseApp);
/*---------------------------------------------------------*/
const formulario = document.getElementById("formulario");
const emailInput = document.getElementById("email");
const passwordInput = document.getElementById("password");
const signInButton = document.getElementById("button");
const errorMensaje = document.getElementById("errorMensaje");
/*---------------------------------------------------------*/
function mostrarMensaje(mensaje) {
  var mensajeElement = document.getElementById('mensaje');
  mensajeElement.textContent = mensaje;
  mensajeElement.style.display = 'block';
  setTimeout(() => {
    mensajeElement.style.display = 'none';
  }, 10000);
}


formulario.addEventListener("submit", (event) => {
  event.preventDefault();

  const email = emailInput.value;
  const password = passwordInput.value;

  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {

      window.location.href = "/html/opcadmin.html";
    })
    .catch((error) => {
      mostrarMensaje("El correo o la contraseña son incorrectas");
    });
});

emailInput.addEventListener("click", () => {
  errorMensaje.textContent = "";
});

passwordInput.addEventListener("click", () => {
  errorMensaje.textContent = "";
});
/*---------------------------------------------------------*/



  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
  import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";
  import { getStorage, ref, uploadBytesResumable, getDownloadURL } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";

  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyC63_-sO6wOagfb8UkyPvsOJgoPmDAp_HQ",
    authDomain: "proyectofinaljoseluis.firebaseapp.com",
    projectId: "proyectofinaljoseluis",
    storageBucket: "proyectofinaljoseluis.appspot.com",
    messagingSenderId: "217855224157",
    appId: "1:217855224157:web:e2ef12a6b055df273da0b4"
  };



const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage(app);

/*--------------------------------------------------- */
window.addEventListener('DOMContentLoaded', (event) => {
  Listarproductos();
});
/*--------------------------------------------------- */
var btnAgregar = document.getElementById('btnAgregar');
var btnBuscar = document.getElementById('btnBuscar');
var btnActualizar = document.getElementById('btnActualizar');
var btnBorrar = document.getElementById('btnBorrar');

const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

var codigo = 0;
var nombrePro = "";
var precioPro = 0;
var descripcionPro = "";
var urlImg = "";

/*--------------------------------------------------- */

uploadButton.addEventListener('click', (event) => {
  event.preventDefault();
  const file = imageInput.files[0];

  if (file) {
    const storageRef = ref(storage, file.name);
    const uploadTask = uploadBytesResumable(storageRef, file);
    uploadTask.on('state_changed', (snapshot) => {
      const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      progressDiv.textContent = 'Progreso: ' + progress.toFixed(2) + '%';
    }, (error) => {
      console.error(error);
    }, () => {
      getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
        txtUrlInput.value = downloadURL; 
        setTimeout(() => {
          progressDiv.textContent = '';
        }, 500);
      }).catch((error) => {
        console.error(error);
      });
    });
  }
});
/*--------------------------------------------------- */

function leerInputs() {
  codigo = document.getElementById('codigo').value;
  nombrePro = document.getElementById('nombre').value;
  precioPro = document.getElementById('precio').value;
  descripcionPro = document.getElementById('descripcion').value;
  urlImg = document.getElementById('txtUrl').value;
}

/*--------------------------------------------------- */

function mostrarMensaje(mensaje) {
  var mensajeElement = document.getElementById('mensaje');
  mensajeElement.textContent = mensaje;
  mensajeElement.style.display = 'block';
  setTimeout(() => {
    mensajeElement.style.display = 'none';
  }, 10000);
}

/*--------------------------------------------------- */

function insertarProducto() {
  leerInputs();
  if (codigo === "" || nombrePro === "" || precioPro === "" || descripcionPro === ""| urlImg === "") {
    mostrarMensaje("Favor de capturar toda la información.");
    return;
  }
  set(refS(db, 'servicios/' + codigo), {
    nombre: nombrePro,
    precio: precioPro,
    descripcion: descripcionPro,
    UrlImg: urlImg
  }).then(() => {
    mostrarMensaje("Se insertó con éxito.");
    limpiarInputs();
    Listarproductos();
  }).catch((error) => {
    mostrarMensaje("Ocurrió un error: " + error);
  });
}


/*--------------------------------------------------- */

function buscarProducto() {
  let codigo = document.getElementById('codigo').value.trim();
  if (codigo === "") {
    mostrarMensaje("No se ingresó código.");
    return;
  }

  const dbref = refS(db);
  get(child(dbref, 'servicios/' + codigo)).then((snapshot) => {
    if (snapshot.exists()) {
      nombrePro = snapshot.val().nombre;
      precioPro = snapshot.val().precio;
      descripcionPro = snapshot.val().descripcion;
      urlImg = snapshot.val().UrlImg;
      escribirInputs();
    } else {
      limpiarInputs();
      mostrarMensaje("El producto con código " + codigo + " no existe.");
    }
  });
}

/*--------------------------------------------------- */

function escribirInputs() {
  document.getElementById('nombre').value = nombrePro;
  document.getElementById('precio').value = precioPro;
  document.getElementById('descripcion').value = descripcionPro;
  document.getElementById('txtUrl').value = urlImg;
}

/*--------------------------------------------------- */

function Listarproductos() {
  const dbRef = refS(db, 'servicios');
  const tabla = document.getElementById('tablaServicios');
  const tbody = tabla.querySelector('tbody');
  tbody.innerHTML = '';

  onValue(dbRef, (snapshot) => {
    snapshot.forEach((childSnapshot) => {
      const childKey = childSnapshot.key;
      const data = childSnapshot.val(); 

      var fila = document.createElement('tr');

      var celdaCodigo = document.createElement('td');
      celdaCodigo.textContent = childKey;
      fila.appendChild(celdaCodigo);

      var celdaNombre = document.createElement('td');
      celdaNombre.textContent = data.nombre;
      fila.appendChild(celdaNombre);

      var celdaPrecio = document.createElement('td');
      celdaPrecio.textContent = "$" + data.precio;
      fila.appendChild(celdaPrecio);

      var celdaDescripcion = document.createElement('td');
      celdaDescripcion.textContent = data.descripcion;
      fila.appendChild(celdaDescripcion);

      var celdaImagen = document.createElement('td');
      var imagen = document.createElement('img');
      imagen.src = data.UrlImg;
      imagen.width = 100;
      celdaImagen.appendChild(imagen);
      fila.appendChild(celdaImagen);

      tbody.appendChild(fila);
    });
  }, { onlyOnce: true });
}

/*--------------------------------------------------- */

function actualizarProducto() {
  leerInputs();
  if (codigo === "" || nombrePro === "" || precioPro === "" || descripcionPro === ""| urlImg === "") {
    mostrarMensaje("Favor de capturar toda la información.");
    return;
  }
  set(refS(db, 'servicios/' + codigo), {
    nombre: nombrePro,
    precio: precioPro,
    descripcion: descripcionPro,
    UrlImg: urlImg
  }).then(() => {
    mostrarMensaje("Se actualizó la información.");
    limpiarInputs();
    Listarproductos();
  }).catch((error) => {
    mostrarMensaje("Ocurrió un error: " + error);
  });
}


/*--------------------------------------------------- */

function eliminarProducto() {
  let codigo = document.getElementById('codigo').value.trim();
  if (codigo === "") {
    mostrarMensaje("No se ingresó un Codigo válido.");
    return;
  }
  const dbref = refS(db);
  get(child(dbref, 'servicios/' + codigo)).then((snapshot) => {
    if (snapshot.exists()) {
      remove(refS(db, 'servicios/' + codigo))
        .then(() => {
          mostrarMensaje("Servicio eliminado con éxito.");
          limpiarInputs();
          Listarproductos();
        })
        .catch((error) => {
          console.log("Ocurrió un error al eliminar el producto: " + error);
        });
    } else {
      limpiarInputs();
      mostrarMensaje("El producto con ID " + codigo + " no existe.");
    }
  });
}

/*--------------------------------------------------- */


function limpiarInputs() {
  document.getElementById('codigo').value = '';
  document.getElementById('nombre').value = '';
  document.getElementById('precio').value = '';
  document.getElementById('descripcion').value = '';
  document.getElementById('txtUrl').value = '';
}

/*--------------------------------------------------- */

function validarNumeros(event) {
  var charCode = event.which ? event.which : event.keyCode;
  if (charCode < 48 || charCode > 57) {
    event.preventDefault();
  }
}

document.getElementById('codigo').addEventListener('keypress', validarNumeros);
document.getElementById('precio').addEventListener('keypress', validarNumeros);

/*--------------------------------------------------- */

btnBorrar.addEventListener('click', eliminarProducto);
btnAgregar.addEventListener('click', insertarProducto);
btnActualizar.addEventListener('click', actualizarProducto);
btnBuscar.addEventListener('click', buscarProducto);


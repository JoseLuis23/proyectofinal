  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
  import { getDatabase, onValue, ref} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";
  

  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyC63_-sO6wOagfb8UkyPvsOJgoPmDAp_HQ",
    authDomain: "proyectofinaljoseluis.firebaseapp.com",
    projectId: "proyectofinaljoseluis",
    storageBucket: "proyectofinaljoseluis.appspot.com",
    messagingSenderId: "217855224157",
    appId: "1:217855224157:web:e2ef12a6b055df273da0b4"
  };


const app = initializeApp(firebaseConfig);
const db = getDatabase(app);


window.addEventListener('DOMContentLoaded', (event) => {
  mostrarProductosHTML();
});

function mostrarProductosHTML() {
    const dbRef = ref(db, 'servicios');
    const section = document.querySelector('.servicios');

    onValue(dbRef, (snapshot) => {
        section.innerHTML = ''; 

        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const data = childSnapshot.val();

            console.log('Nombre del producto:', data.nombre);
            console.log('Precio del producto:', data.precio);
            console.log('Descripción del producto:', data.descripcion);

        const serviceDiv = document.createElement('div');
        serviceDiv.className = 'service';
  
        const productImage = document.createElement('img');
        productImage.src = data.UrlImg;
        productImage.alt = '';
        serviceDiv.appendChild(productImage);
  
        const serviceName = document.createElement('h3');
        serviceName.className = 'name';
        serviceName.textContent = data.nombre;
        serviceDiv.appendChild(serviceName);
  
        const servicePrice = document.createElement('p');
        servicePrice.className = 'price';
        servicePrice.textContent = `$${data.precio}`;
        serviceDiv.appendChild(servicePrice);
  
        const serviceDesc = document.createElement('p');
        serviceDesc.className = 'desc';
        serviceDesc.textContent = data.descripcion; // Corregir esta línea
        serviceDiv.appendChild(serviceDesc);
  
  
        section.appendChild(serviceDiv);
      });
    }, { onlyOnce: true });
  }
